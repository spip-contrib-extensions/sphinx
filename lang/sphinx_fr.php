<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/sphinx/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'query_relax' => 'La recherche sur les mots « @mots@ » ne donne aucun résultat ; voici les résultats contenant l’un ou l’autre de ces mots.',
);

?>
